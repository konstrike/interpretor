package controller;

import domain.PrgState;
import domain.adt.MyIStack;
import domain.statements.IStmt;
import exception.MyException;
import repository.IRepository;
import repository.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class Controller {
    IRepository myRepository;

    public Controller(Repository myRepository)
    {
        this.myRepository=myRepository;
    }

    public void addProgram(PrgState newPrg)
    {
        myRepository.addPrg(newPrg);
    }


    Map<Integer,Integer> conservativeGarbageCollector(Collection<Integer> symTableValues, Map<Integer,Integer> heap)
    {
        return heap.entrySet().stream().
                filter(e->symTableValues.contains(e.getKey())).
                        collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }


    public PrgState oneStep(PrgState state) throws MyException
    {
        MyIStack<IStmt> stack = state.getExeStack();
        if(stack.isEmpty()) throw new MyException("Reached the end!");
        IStmt crtStmt=stack.pop();
        return crtStmt.execute(state);
    }

    public void allStep()
    {
        PrgState prg=myRepository.getCrtPrg();
        try{
            while(true){
                System.out.println(prg.toString());
                myRepository.addPrg(prg);
                myRepository.logPrgStateExec();
                oneStep(prg);
                prg.getHeap().setContent(conservativeGarbageCollector(
                        prg.getSymTable().getContent().values(),
                        prg.getHeap().getContent()));
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }
}
