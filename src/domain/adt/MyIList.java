package domain.adt;

public interface MyIList<T> {
    void add(T v);
    T pop();
    String toString();
}
