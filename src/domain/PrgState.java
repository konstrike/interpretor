package domain;

import domain.adt.*;
import domain.statements.IStmt;

import java.io.BufferedReader;

public class PrgState {
    MyIStack<IStmt> exeStack;
    MyIDictionary<String,Integer> symTable;
    MyIList<Integer> out;
    IStmt originalProgram;
    MyIDictionary<Integer,MyTuple<String, BufferedReader>> fileTable;
    MyIHeap<Integer,Integer> heap;

    public PrgState(MyIStack<IStmt> stack,MyIDictionary<String,Integer> symTable,MyIList<Integer> out,IStmt program,MyIDictionary<Integer,MyTuple<String, BufferedReader>> fileTable,MyIHeap<Integer,Integer> heap){
        this.exeStack=stack;
        this.symTable=symTable;
        this.out=out;
        this.originalProgram=program;
        this.fileTable=fileTable;
        this.heap=heap;
        exeStack.push(program);
    }

    public MyIStack<IStmt> getExeStack() {
        return exeStack;
    }

    public MyIDictionary<Integer,MyTuple<String, BufferedReader>> getFileTable() {return this.fileTable;}

    public MyIHeap<Integer,Integer> getHeap() {return this.heap;}

    public void setExeStack(MyIStack<IStmt> exeStack) {
        this.exeStack = exeStack;
    }

    public MyIDictionary<String, Integer> getSymTable() {
        return symTable;
    }

    public void setSymTable(MyIDictionary<String, Integer> symTable) {
        this.symTable = symTable;
    }

    public MyIList<Integer> getOut() {
        return out;
    }

    public void setOut(MyIList<Integer> out) {
        this.out = out;
    }

    public IStmt getOriginalProgram() {
        return originalProgram;
    }

    public void setOriginalProgram(IStmt originalProgram) {
        this.originalProgram = originalProgram;
    }

    public String toString(){
        return "ExeStack:\n"+exeStack.toString()+"Sym Table:\n"+symTable.toString()+"Print output:\n"+out.toString()+"File Table:\n"+fileTable.toString()+"Heap:\n"+heap.toString();
    }
}
