package domain.statements;

import domain.PrgState;
import domain.adt.MyIDictionary;
import domain.adt.MyIHeap;
import domain.expressions.Exp;
import exception.MyException;

public class HeapWriteStmt implements IStmt{

    private String var;
    private Exp expression;

    public HeapWriteStmt(String var,Exp expression){this.var=var;this.expression=expression;}

    @Override
    public PrgState execute(PrgState state) throws MyException {
        MyIHeap<Integer,Integer> heap=state.getHeap();
        MyIDictionary<String,Integer> symTable=state.getSymTable();
        int val=expression.eval(symTable,heap);
        int Hpoz=symTable.lookup(var);
        heap.update(Hpoz,val);
        return state;
    }

    public String toString()
    {
        return "wH("+var+","+expression.toString()+")";
    }
}
