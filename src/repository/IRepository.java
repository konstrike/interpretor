package repository;

import domain.PrgState;


public interface IRepository {
    void addPrg(PrgState newPrg);
    void logPrgStateExec();
    PrgState getCrtPrg();
}
