package view;

import controller.Controller;
import domain.PrgState;
import domain.adt.*;
import domain.statements.*;
import domain.expressions.*;
import repository.Repository;

import java.io.BufferedReader;

public class View {
    static Repository myRepository=new Repository("ceva.txt");
    static Controller myController=new Controller(myRepository);

    public static void main(String[] args)
    {
        //openRFile(var_f,"test.in");
        //readFile(var_f,var_c);print(var_c);
        //(if var_c then readFile(var_f,var_c);print(var_c)
        //else print(0));
        //closeRFile(var_f)

        //IStmt originalProgram=new IfStmt(new ConstExp(10), new CompStmt(new AssignStmt("v", new ConstExp(5)), new PrintStmt(new ArithExp('/', new VarExp("v"), new ConstExp(3)))), new PrintStmt(new ConstExp(100)));
        //IStmt originalProgram1=new CompStmt(new AssignStmt("v",new ConstExp(2)),new PrintStmt(new VarExp("v")));
        //IStmt originalProgram2=new CompStmt(new AssignStmt("a",new ArithExp('+',new ConstExp(2),new ArithExp('*',new ConstExp(3),new ConstExp(5)))),new CompStmt(new AssignStmt("b",new ArithExp('+',new VarExp("a"),new ConstExp(1))),new PrintStmt(new VarExp("b"))));
//        IStmt originalProgram3=new CompStmt(new AssignStmt("a",new ArithExp('+',new ConstExp(2),new ConstExp(2))),new CompStmt(new IfStmt(new VarExp("a"),new AssignStmt("v",new ConstExp(2)),new AssignStmt("v",new ConstExp(3))),new PrintStmt(new VarExp("v"))));
//        IStmt originalProgram4=new CompStmt(new OpenRFile("t1","test.txt"),new CompStmt(new ReadFile(new VarExp("t1"),"ceva"),new CompStmt(new ReadFile(new VarExp("t1"),"altceva"),new PrintStmt(new VarExp("ceva")))));
//
//        MyIStack<IStmt> exeStack=new MyStack<IStmt>();
//        MyIDictionary<String,Integer> symTable=new MyDictionary<String, Integer>();
//        MyIList<Integer> out=new MyList<Integer>();
//        MyIDictionary<Integer,MyTuple<String, BufferedReader>> fileTable=new MyDictionary<Integer,MyTuple<String, BufferedReader>>();
//        MyIHeap<Integer,Integer> heap=new MyHeap<Integer, Integer>();
//        PrgState myPrgState =new PrgState(exeStack,symTable,out,originalProgram4,fileTable,heap);
        //myController.addProgram(myPrgState);
        //myController.allStep();
        Interpreter interp=new Interpreter();
        interp.main();
    }
}
