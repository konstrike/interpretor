package view;

import controller.Controller;
import domain.PrgState;
import domain.adt.*;
import domain.expressions.*;
import domain.statements.IStmt;
import domain.statements.*;
import domain.statements.CloseRFile;
import repository.IRepository;
import repository.Repository;

import java.io.BufferedReader;

class Interpreter {
public static void main() {

        IStmt ex1=new IfStmt(new ConstExp(10), new CompStmt(new AssignStmt("v", new ConstExp(5)), new PrintStmt(new ArithExp('/', new VarExp("v"), new ConstExp(3)))), new PrintStmt(new ConstExp(100)));;

        MyIStack<IStmt> exeStack=new MyStack<IStmt>();
        MyIDictionary<String,Integer> symTable=new MyDictionary<String, Integer>();
        MyIList<Integer> out=new MyList<Integer>();
        MyIDictionary<Integer,MyTuple<String, BufferedReader>> fileTable=new MyDictionary<Integer,MyTuple<String, BufferedReader>>();
        MyIHeap<Integer,Integer> heap=new MyHeap<Integer, Integer>();

        PrgState prg1 = new PrgState(exeStack,symTable,out,ex1,fileTable,heap);

        Repository repo1 = new Repository("log1.txt");
        Controller ctr1 = new Controller(repo1);
        ctr1.addProgram(prg1);


        IStmt ex2=new CompStmt(new OpenRFile("t1","test.txt"),new CompStmt(new ReadFile(new VarExp("t1"),"ceva"),new CompStmt(new ReadFile(new VarExp("t1"),"altceva"),new CloseRFile(new VarExp("t1")))));

        MyIStack<IStmt> exeStack1=new MyStack<IStmt>();
        MyIDictionary<String,Integer> symTable1=new MyDictionary<String, Integer>();
        MyIList<Integer> out1=new MyList<Integer>();
        MyIDictionary<Integer,MyTuple<String, BufferedReader>> fileTable1=new MyDictionary<Integer,MyTuple<String, BufferedReader>>();
        MyIHeap<Integer,Integer> heap1=new MyHeap<Integer, Integer>();

        PrgState prg2 = new PrgState(exeStack1,symTable1,out1,ex2,fileTable1,heap1);
        Repository repo2 = new Repository("log2.txt");
        Controller ctr2 = new Controller(repo2);
        ctr2.addProgram(prg2);


        IStmt ex3=new CompStmt(new AssignStmt("v",new ConstExp(10)),new CompStmt(new HeapAlocStmt("v",new ConstExp(20)),new CompStmt(new HeapAlocStmt("a",new ConstExp(22)),new PrintStmt(new HeapReadExp("v")))));

        MyIStack<IStmt> exeStack2=new MyStack<IStmt>();
        MyIDictionary<String,Integer> symTable2=new MyDictionary<String, Integer>();
        MyIList<Integer> out2=new MyList<Integer>();
        MyIDictionary<Integer,MyTuple<String, BufferedReader>> fileTable2=new MyDictionary<Integer,MyTuple<String, BufferedReader>>();
        MyIHeap<Integer,Integer> heap2=new MyHeap<Integer, Integer>();

        PrgState prg3 = new PrgState(exeStack2,symTable2,out2,ex3,fileTable2,heap2);
        Repository repo3 = new Repository("log3.txt");
        Controller ctr3 = new Controller(repo3);
        ctr3.addProgram(prg3);

        IStmt ex4=new CompStmt(new AssignStmt("v",new ConstExp(10)),new CompStmt(new HeapAlocStmt("v",new ConstExp(20)),new CompStmt(new HeapAlocStmt("a",new ConstExp(22)),new CompStmt(new HeapWriteStmt("a",new ConstExp(30)),new CompStmt(new PrintStmt(new VarExp("a")),new PrintStmt(new HeapReadExp("a")))))));

        MyIStack<IStmt> exeStack3=new MyStack<IStmt>();
        MyIDictionary<String,Integer> symTable3=new MyDictionary<String, Integer>();
        MyIList<Integer> out3=new MyList<Integer>();
        MyIDictionary<Integer,MyTuple<String, BufferedReader>> fileTable3=new MyDictionary<Integer,MyTuple<String, BufferedReader>>();
        MyIHeap<Integer,Integer> heap3=new MyHeap<Integer, Integer>();

        PrgState prg4 = new PrgState(exeStack3,symTable3,out3,ex4,fileTable3,heap3);
        Repository repo4 = new Repository("log4.txt");
        Controller ctr4 = new Controller(repo4);
        ctr4.addProgram(prg4);

        TextMenu menu = new TextMenu();
        menu.addCommand(new ExitCommand("0", "exit"));
        menu.addCommand(new RunExample("1",ex1.toString(),ctr1));
        menu.addCommand(new RunExample("2",ex2.toString(),ctr2));
        menu.addCommand(new RunExample("3",ex3.toString(),ctr3));
        menu.addCommand(new RunExample("4",ex4.toString(),ctr4));
        menu.show();
        }}
